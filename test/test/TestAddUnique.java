/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import taskmanager.Task;
import taskmanager.TaskList;

/**
 *
 * @author LmThu
 */
public class TestAddUnique {

    private TaskList t;

    public TestAddUnique() {
    }

    @Before
    public void setUp() {
        t = new TaskList();

    }

    @After
    public void tearDown() {
        t = null;
    }

    @Test
    public void testAddUnique() {
        t.addNewTask(new Task());
        t.addNewTask(new Task());

        assertFalse(t.getTask().get(0).getTaskId().equals(t.getTask().get(1).getTaskId()));
    }

    @Test
    public void testAddUnique1() {
        t.addNewTask(new Task());
        t.addNewTask(new Task());
        t.addNewTask(new Task());

        assertFalse(t.getTask().get(1).getTaskId().equals(t.getTask().get(2).getTaskId()));
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
