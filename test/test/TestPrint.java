/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import taskmanager.Task;
import taskmanager.TaskList;

/**
 *
 * @author LmThu
 */
public class TestPrint {

    private TaskList t;

    public TestPrint() {
    }

    @Before
    public void setUp() {
        t = new TaskList();
        t.addNewTask(new Task());
        t.addNewTask(new Task());
        t.addNewTask(new Task());
    }

    @After
    public void tearDown() {
        t = null;

    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testPrint() {
        System.out.println(t);
        assertTrue(t.toString().length() > 0);
    }

    @Test
    public void testPrintAll() {
        fail("Not yet implemented");
    }

    @Test
    public void testPrintByLeader() {
        fail("Not yet implemented");
    }
    @Test
    public void test1Ongoing() {
        List<Task> tS = t.getTaskByStatus();
        assertTrue(tS.size() > 0 && tS.size() <= t.getSize());
        
//        fail("Not yet implemented");
    }
    @Test
    public void testPrintByDateRange() {
        fail("Not yet implemented");
    }
}
