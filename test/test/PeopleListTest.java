/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import taskmanager.PeopleList;
import taskmanager.Person;

/**
 *
 * @author LmThu
 */
public class PeopleListTest {

    private PeopleList pL;

    public PeopleListTest() {
    }

    @Before
    public void setUp() {
        pL = new PeopleList();
        pL.addNewPerson(new Person("Joe", "joe123@gmail.com", "012314669"));
        pL.addNewPerson(new Person("Thu", "leminthu96@gmail.com", "01215159946"));
        pL.addNewPerson(new Person("Derv", "derv1@hotmail.com", "21221554"));

    }

    @Test
    public void testAdd() {
        int oldSize = pL.getSize();
        pL.addNewPerson(new Person());
        assertTrue(pL.getSize() > oldSize);
    }

    @Test
    public void testToString() {
        assertTrue(pL.toString().length() > 0);
    }

    @Test
    public void testEdit() {
        assertTrue(pL.editEmployeeNameByID("EMP1", "Thu"));

    }

    @Test
    public void testprintAll() {
        fail("void Method");
    }

    @Test
    public void testGetEmployeeInfoByID() {
        assertTrue(pL.getEmployeeByID("EMP1")==0);
    }

    @Test
    public void testRemove() {
        fail("void method");
    }

}
