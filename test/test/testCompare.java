/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.Calendar;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import taskmanager.Task;
import taskmanager.TaskList;

/**
 *
 * @author LmThu
 */
public class testCompare {

    private TaskList t;

    public testCompare() {
    }

    @Before
    public void setUp() {
        t = new TaskList();
        Calendar day = Calendar.getInstance();
        day.set(2016, 1, 3);
        t.addNewTask(new Task(null, null, null, Calendar.getInstance(), day));
        day.set(2016, 3, 5);
        t.addNewTask(new Task(null, null, null, Calendar.getInstance(), day));
        day.set(2016, 1, 5);
        t.addNewTask(new Task(null, null, null, Calendar.getInstance(), day));

    }

    @After
    public void tearDown() {
        t = null;
    }

    @Test
    public void testAverage() {
        assertTrue(t.getAverageTime() > 0);
    }

    @Test
    public void testCompare() {
        assertFalse(t.getTask().get(0).equals(t.getTask().get(1)));
    }

}
