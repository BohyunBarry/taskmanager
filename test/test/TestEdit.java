/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import taskmanager.Status;
import taskmanager.Task;
import taskmanager.TaskList;

/**
 *
 * @author LmThu
 */
public class TestEdit {

    private TaskList t;

    public TestEdit() {
    }

    @Before
    public void setUp() {
        t = new TaskList();
        t.addNewTask(new Task());
        t.addNewTask(new Task());
        t.addNewTask(new Task());

    }

    @After
    public void tearDown() {
        t = null;
    }

    @Test
    public void testEdit() {
        
        t.getTask().get(0).setTaskName("New Task Name");
        assertFalse(t.getTask().get(0).getTaskName().equals(t.getTask().get(1).getTaskName()));
    }
    
    @Test
    public void testEdit1() {
        
        t.getTask().get(1).setTaskName("New Task Name 1");
        assertFalse(t.getTask().get(1).getTaskName().equals(t.getTask().get(2).getTaskName()));
    }
    @Test
    public void testEdit2() {
        
        t.getTask().get(1).setStatus(Status.PAUSED);
        assertFalse(t.getTask().get(1).getStatus().equals(t.getTask().get(2).getStatus()));
    }
    
    @Test
    public void testEdit3() {
        
        t.getTask().get(0).setStatus(Status.PAUSED);
        assertFalse(t.getTask().get(0).getStatus().equals(t.getTask().get(1).getStatus()));
    }
}
