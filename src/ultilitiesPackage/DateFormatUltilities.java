/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ultilitiesPackage;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author LmThu
 */
public class DateFormatUltilities {

    public static int getDateYear(Calendar date) {
        return date.YEAR;
    }

    public static int getDateDay(Calendar date) {
        return date.DATE;
    }

    public static int getDateMonth(Calendar date) {
        return date.MONTH + 1;
    }

    public static int getDateHour(Calendar date) {
        return date.HOUR_OF_DAY;
    }

    public static String getDateAsString(Calendar date) {
        SimpleDateFormat defaultFormat = new SimpleDateFormat("dd-MM-yyyy");
        return defaultFormat.format(date.getTime());
    }

    public static String getDateAsString(Calendar date, String formatter) {
        SimpleDateFormat _format = new SimpleDateFormat(formatter);
        return _format.format(date.getTime());
    }

    public static Calendar setDate(int year, int month, int day, int hour, int minute, int second) {
        Calendar date = Calendar.getInstance();
        date.set(year, month-1, day, hour, minute, second);
        return date;
    }

    public static Calendar setDate(int year, int month, int day) {
        Calendar date = Calendar.getInstance();
        date.set(year, month-1, day);
        return date;
    }

    public static long getDifference(Calendar date1, Calendar date2) {
        return (date1.getTimeInMillis() - date2.getTimeInMillis());
    }


    public static Calendar getDateFromString(String s) {

        int year;
        int month;
        int day;
        String pattern = "(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d)";
        Matcher m = Pattern.compile(pattern).matcher(s);
        if (m.find()) {
            year=Integer.parseInt(m.group(1));
            month=Integer.parseInt(m.group(2));
            day=Integer.parseInt(m.group(3));
        } else {
            return Calendar.getInstance();
        }
        return setDate(year, month, day);
    }
    
}
