/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ultilitiesPackage;

import java.util.Scanner;

/**
 *
 * @author LmThu
 */
public class InputUltilities {

    static Scanner kb = new Scanner(System.in);

    public static boolean getNextBool(String mes) {
        System.out.println(mes + ": ");
        boolean input = kb.nextBoolean();
        kb.nextLine();
        return input;
    }

    public static String getNextString(String mes) {
        System.out.println(mes + ": ");
        String input;
        do {
            input = kb.nextLine();
        } while (input.length() == 0);
        return input;
    }

    public static int getNextInt(String mes) {
        System.out.println(mes + ": ");
        int input = kb.nextInt();
        kb.nextLine();
        return input;
    }

    public static int getNextInt(String mes, int min, int max) {
        int input;
        do {
            System.out.println(mes + ": ");
            input = kb.nextInt();
            kb.nextLine();
        } while (input < min || input > max);
        return input;
    }

    public static byte getNextByte(String mes) {
        System.out.println(mes + ": ");
        byte input = kb.nextByte();
        kb.nextLine();
        return input;
    }

    public static byte getNextByte(String mes, byte min, byte max) {
        byte input;
        do {
            System.out.println(mes + ":");
            input = kb.nextByte();
            kb.nextLine();

        } while (input < min || input > max);
        return input;
    }

    public static short getNextShort(String mes) {
        System.out.println(mes + ": ");
        short input = kb.nextShort();
        kb.nextLine();
        return input;
    }

    public static short getNextShort(String mes, short min, short max) {
        short input;
        do {
            System.out.println(mes + ": ");
            input = kb.nextShort();
            kb.nextLine();

        } while (input < min || input > max);
        return input;
    }

    public static float getNextFloat(String mes) {
        System.out.println(mes);
        float input = kb.nextFloat();
        kb.nextLine();
        return input;
    }

    public static float getNextFloat(String mes, float min, float max) {
        float input;
        do {
            System.out.println(mes + ": ");
            input = kb.nextFloat();
            kb.nextLine();

        } while (input < min || input > max);
        return input;
    }

    public static double getNextDouble(String mes) {
        System.out.println(mes + ": ");
        double input = kb.nextDouble();
        kb.nextLine();
        return input;
    }

    public static double getNextDouble(String mes, double min, double max) {
        double input;
        do {
            System.out.println(mes + ": ");
            input = kb.nextDouble();
            kb.nextLine();
        } while (input < min || input > max);
        return input;
    }

    public static String getNextDate(String mes) {
        String input;
        boolean valid = false;
        do {
            System.out.println(mes + ": ");
            input = kb.nextLine();
            if (input.length() == 10) {
                if (ValidationUltilities.isNumeric(input.substring(0, 4))) {
                    if (ValidationUltilities.isNumeric(input.substring(5, 7))) {
                        if (ValidationUltilities.isNumeric(input.substring(8))) {
                            valid=true;
                        }
                    }
                }
            }
        } while (!valid);
        return input.substring(0, 4)+"-"+input.substring(5, 7)+"-"+input.substring(8);
    }
}
