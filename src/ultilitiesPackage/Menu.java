/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ultilitiesPackage;

import java.util.ArrayList;

/**
 *
 * @author LmThu
 */
public class Menu {

    private String name;
    private ArrayList<String> options = new ArrayList<>();
    private int escapeKey;
    
    private String description;

    public Menu() {
        name = "";
        options.add("Exit");
        escapeKey = 0;
    }

    public Menu(String name, String description) {
        this.name = name;
        this.description=description;
        options.add("Exit");
        escapeKey = 0;
    }
    
    public Menu(String name, String keyname,String description) {
        this.description=description;
        this.name = name;
        options.add(keyname);
        escapeKey = 0;
    }

    public int getEscapeKey()
    {
        return escapeKey;
    }
    
    public String getName()
    {
        return name;
    }
    public void addOption(String option) {
        if (option.length() > 0 && (!options.contains(option))) {
            options.add(option);
        } else {
            System.out.println("Please check the option");
        }
    }

    public boolean removeOption(int index) {
        if (index >= options.size()) {
            return false;
        } else {
            options.remove(index);
            return true;
        }
    }

    public int getSize() {
        return options.size();
    }

    public boolean removeFromMenu(int index) {
        if (index < options.size()) {
            options.remove(index);
            return true;
        } else {
            return false;
        }
    }

    public void clear() {
        options.clear();
    }

    public String toString() {
        String temp = name+"\r\n"+description+"\r\n";
        int i=0;
        for (String option : options) {
            temp += i+". "+ option + "\r\n";
            i++;
        }
        return temp;
    }

    public int getInput() {
        int input;
        input = InputUltilities.getNextInt("Please Enter your Selection",0, options.size()-1);
        return input;
    }
    
}
