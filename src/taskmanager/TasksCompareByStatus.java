/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager;

import java.util.Comparator;

/**
 *
 * @author LmThu
 */
public class TasksCompareByStatus implements Comparator<Task> {

    @Override
    public int compare(Task o1, Task o2) {
        return o1.getStatus().compareTo(o2.getStatus());

    }

}
