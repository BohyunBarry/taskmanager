/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author LmThu
 */
public class Person implements Cloneable, Serializable {

    private static final long serialVersionUID = -8146269383903353926L;

    private static int lastId = 0;
    private String name;
    private String id;
    private String email;
    private String tel;

    public Person() {
        name = "Not Available";
        lastId++;
        id = "EMP" + lastId;
        email = "";
        tel = "";
    }

    public Person(String name, String email, String tel) {
        this.name = name;
        lastId++;
        this.id = "EMP" + lastId;
        this.email = email;
        this.tel = tel;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String tel() {
        return tel;
    }

    public void setName(String name) {
        if (name.length() > 0) {
            this.name = name;
        } else {
            this.name = "NA";
        }
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "{Name: " + name + ", ID: " + id + ", Email: " + email + ", Tel: " + tel + "}";
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof Person)) {
            return false;
        } else {
            Person pTemp = (Person) obj;
            return this.id.equals(pTemp.getId()) && this.name.equals(pTemp.getName());
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
