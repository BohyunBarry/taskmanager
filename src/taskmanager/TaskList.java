/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ultilitiesPackage.DateFormatUltilities;
import ultilitiesPackage.InputUltilities;

/**
 *
 * @author LmThu
 */
public class TaskList implements Serializable, Cloneable {

    private static final long serialVersionUID = -8391388585603023735L;

    private List<Task> tasksList = new ArrayList<>();

    public TaskList() {
    }

    public TaskList(List<Task> tasksList) {
        this.tasksList = tasksList;
    }

    public int getSize() {
        return tasksList.size();
    }

    public List<Task> getTask() {
        return tasksList;
    }

    @Override
    public String toString() {
        String temp = "{TaskList";
        for (Task p : tasksList) {
            temp += p;
        }
        temp += "}\r\n";
        return temp;
    }

    public Task makeNewTask(PeopleList resource) {
        PeopleList team = new PeopleList();
        String taskName = InputUltilities.getNextString("Please Enter the Task Name");
        Person taskLeader = findExistingPerson("Please Enter the Task Leader", resource);
        int numberOfMember = InputUltilities.getNextInt("Please Enter the number of member", 0, 100);
        for (int i = 0; i < numberOfMember; i++) {
            team.addNewPerson(findExistingPerson("Please Enter the Team Member", resource));
        }
        Calendar startOnDate = Calendar.getInstance();
        String dueDateString = "";
        Calendar dueOnDate = Calendar.getInstance();
        do {
            dueDateString = InputUltilities.getNextDate("Please Enter the due date yyyy-mm-dd");
            dueOnDate = DateFormatUltilities.getDateFromString(dueDateString);
        } while (DateFormatUltilities.getDifference(dueOnDate, startOnDate) < 0);
        return new Task(taskName, taskLeader, team.getList(), startOnDate, dueOnDate);
    }

    public boolean addNewTask(Task t) {
        if (!tasksList.contains(t)) {
            tasksList.add(t);
            return true;
        } else {
            System.out.println("Duplicated");
            return false;
        }
    }

    public Person findExistingPerson(String mes, PeopleList resource) {
        String personID = "";
        int index = 0;
        do {
            personID = InputUltilities.getNextString(mes);
            index = resource.getEmployeeByID(personID);
        } while (index == -1);
        return resource.getList().get(index);
    }

    public int getTaskByID(String id) {
        int i = 0;
        do {
            if (!(id.equals(tasksList.get(i).getTaskId()))) {
                i++;
            }
        } while ((i < tasksList.size()) && (!(id.equals(tasksList.get(i).getTaskId()))));
        if (i == tasksList.size()) {
            return -1;
        } else {
            return i;
        }
    }

    public void printTaskInfoByID(String id) {
        int i = getTaskByID(id);
        if (getTaskByID(id) != -1) {
            System.out.println(tasksList.get(i));
        } else {
            System.out.println("Cant Find the requested Task");
        }
    }

    public void editTaskNameByID(String id, String taskName) {
        int i = getTaskByID(id);
        if (getTaskByID(id) != -1) {
            tasksList.get(i).setTaskName(taskName);
        } else {
            System.out.println("Cant Find the requested Task");
        }
    }

    public void editTaskLeaderByID(String id, Person leader) {
        int i = getTaskByID(id);
        if (getTaskByID(id) != -1) {
            tasksList.get(i).setTaskLeader(leader);
        } else {
            System.out.println("Cant Find the requested Task");
        }
    }

    public void editTaskStatusByID(String id, Status status) {
        int i = getTaskByID(id);
        if (getTaskByID(id) != -1) {
            tasksList.get(i).setStatus(status);
        } else {
            System.out.println("Cant Find the requested Task");
        }
    }

    public void replaceTaskTeamByID(String id, List<Person> team) {
        int i = getTaskByID(id);
        if (i != -1) {
            tasksList.get(i).setTaskTeam(team);
        } else {
            System.out.println("Cant Find the requested Task");
        }
    }

    public void editDueOnDate(String id, Calendar dueDate) {
        int i = getTaskByID(id);
        if (getTaskByID(id) != -1) {
            tasksList.get(i).setDueOnDate(dueDate);
        } else {
            System.out.println("Cant Find the requested Task");
        }
    }

    public boolean removeTaskFromSystem(String id, Calendar dueDate) {
        int i = getTaskByID(id);
        if (getTaskByID(id) != -1) {
            tasksList.remove(i);
            return true;
        } else {
            System.out.println("Cant Find the requested Task");
            return false;
        }
    }

    public void sortTaskByStatus() {
        Collections.sort(tasksList, new TasksCompareByStatus());
    }

    public boolean duplicateTask(int index) {
        try {
            tasksList.add((Task) tasksList.get(index).clone());
            return true;
        } catch (CloneNotSupportedException ex) {

            return false;
        }
    }

    public void printAllTask() {
        if (tasksList.size() > 0) {
            System.out.println(tasksList);
        } else {
            System.out.println("EMPTY");
        }

    }

    public void setAll(List<Task> _tasks) {
        tasksList.addAll(_tasks);
    }

    public void printTaskByLeader() {
        String leaderId = InputUltilities.getNextString("Enter the leader here");
        int i = 0;
        do {
            if (tasksList.get(i).gettaskLeader().getId().equals(leaderId)) {
                System.out.println(tasksList.get(i));
            }
            i++;
        } while (i < tasksList.size());

    }

    public List<Task> getTaskByLeader() {
        List<Task> list = new ArrayList<>();
        String leaderId = InputUltilities.getNextString("Enter the leader here");
        int i = 0;
        do {
            if (tasksList.get(i).gettaskLeader().getId().equals(leaderId)) {
                list.add(tasksList.get(i));
            }
            i++;
        } while (i < tasksList.size());
        return list;
    }

    public List<Task> getTaskByStatus() {
        List<Task> list = new ArrayList<>();
        String status = InputUltilities.getNextString("Enter the STATUS here");

        Status _status = Status.ONGOING;
        switch (status) {
            case "COMPLETED":
                _status = Status.COMPLETED;
                break;
            case "CANCELLED":
                _status = Status.CANCELLED;
                break;
            case "PAUSED":
                _status = Status.PAUSED;
            default:
                _status = Status.ONGOING;
                break;
        }
        int i = 0;
        do {
            if (tasksList.get(i).getStatus().equals(_status)) {
                list.add(tasksList.get(i));
            }
            i++;
        } while (i < tasksList.size());
        return list;
    }

    public List<Task> getTaskByDateRange() {
        List<Task> list = new ArrayList<>();
        String status = InputUltilities.getNextString("Enter the Date here");

        Calendar preset = Calendar.getInstance();
        preset = DateFormatUltilities.getDateFromString(status);

        for (Task p : tasksList) {
            if (DateFormatUltilities.getDifference(preset, p.getDueOnDate()) > 0) {
                list.add(p);
            }
        }
        return list;
    }

    public double getAverageTime() {

        long sum = 0;
        for (Task p : tasksList) {
            sum += DateFormatUltilities.getDifference(p.getDueOnDate(), p.getAddOnDate());
        }
        return (double) sum / tasksList.size() / 1000 / 3600;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public void deleteTaskByID() {
        System.out.println("Please be careful when delete this");
        String id = InputUltilities.getNextString("Enter ID Here");
        int index = getTaskByID(id);
        tasksList.remove(index);
    }

    public List<Task> lateTaskByUser() {
        String leaderID = InputUltilities.getNextString("Please Enter the leader Id");
        List<Task> leaderList = getTaskByLeader();
        List<Task> res = null;
        int numOfDay = InputUltilities.getNextInt("NUmber of days", 0, 100);

        for (Task t : leaderList) {
            if (t.getStatus() != Status.COMPLETED && DateFormatUltilities.getDifference(Calendar.getInstance(),t.getDueOnDate()) > numOfDay*36524) {
                res.add(t);
            }
        }
        return res;
    }

}
