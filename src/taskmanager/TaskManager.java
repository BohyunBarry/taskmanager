/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager;

import ultilitiesPackage.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ultilitiesPackage.DateFormatUltilities;
import ultilitiesPackage.InputUltilities;
import ultilitiesPackage.Menu;
import ultilitiesPackage.ValidationUltilities;
import ultilitiesPackage.YesNoMenu;

/**
 *
 * @author LmThu
 */
public class TaskManager {

    /**
     * @param args the command line arguments
     */
    private PeopleList pList = new PeopleList();
    private TaskList tList = new TaskList();

    public static void main(String[] args) {
        TaskManager ma = new TaskManager();
        ma.start();
    }

    public void start() {
        initialize();
        showMainMenu();
    }

    private void showMainMenu() {
        int temp;
        do {
            System.out.println(mainMenu);
            temp = mainMenu.getInput();
            switch (temp) {
                case 1:
                    showPersonMenu();
                    break;
                case 2:
                    showTaskMenu();
                    break;
                case 3:
                    System.out.println("Search On Task");
                    execSearch();
                    break;
                default:
                    SerializationUtility.save("data/", "taskList.txt", tList);
                    SerializationUtility.save("data/", "peopleList.txt", pList);

                    System.out.println("Good Bye");
                    break;
            }
        } while (temp != mainMenu.getEscapeKey());

    }

    private void showPersonMenu() {

        int temp;
        do {
            System.out.println(personMenu);
            temp = personMenu.getInput();
            switch (temp) {
                case 1:
                    pList.printAllPeople();
                    break;
                case 2:
                    addPerson();
                    break;
                case 3:
                    editPerson();
                    break;
                case 4:
                    exec_deletePerson();
                    break;
                default:
                    System.out.println("Going to home page");
                    break;
            }
        } while (temp != personMenu.getEscapeKey());

    }

    private void addPerson() {
        Person p;
        YesNoMenu continuty = new YesNoMenu("Add more", "Do you Want to Add Employee");

        int temp;
        do {
            System.out.println(continuty);
            temp = continuty.getInput();
            switch (temp) {
                case 1:
                    p = pList.makeNewPerson();
                    pList.addNewPerson(p);
                    break;
                case 2:
                    pList.printAllPeople();
                    break;
                default:
                    System.out.println("Go back");
                    break;
            }
        } while (temp != continuty.getEscapeKey());
    }

    private void editPerson() {
        if (pList.getSize() > 0) {
            System.out.println("EDIT THE INFORMATION OF 1 EMPLOYEE");
            String id = InputUltilities.getNextString("Enter ID here");
            String content = "";
            String field = InputUltilities.getNextString("Enter Field: ");
            switch (field) {
                case "email":
                    content = InputUltilities.getNextString("Enter new Email");
                    pList.editEmployeeEmailByID(id, content);
                    break;
                case "phone":
                    content = InputUltilities.getNextString("Enter new Phone");
                    pList.editEmployeePhoneByID(id, content);
                    break;
                case "name":
                    content = InputUltilities.getNextString("Enter new Name");
                    pList.editEmployeeNameByID(id, content);
                    break;
                default:
                    System.out.println("Field Not Found");
                    break;
            }
            pList.printEmployeeInfoByID(id);
        } else {
            System.out.println("Not Available at the moment");
        }

    }

    private void exec_deletePerson() {
        if (pList.getSize() > 0) {
            System.out.println("PLEASE BE VERY CAREFUL TO EXECUTE THIS QUERY");
            String id = InputUltilities.getNextString("Enter ID here");
            pList.deleteEmployeeInfoByID(id);
            pList.printAllPeople();
        } else {
            System.out.println("Not Available At the Moment");
        }
    }

    private void showTaskMenu() {
        int temp;
        do {
            System.out.println(taskMenu);
            temp = taskMenu.getInput();
            switch (temp) {
                case 1:
                    System.out.println(tList);
                    break;
                case 2:
                    addTask();
                    break;
                case 3:
                    editTask();
                    break;
                case 4:
                    tList.deleteTaskByID();
                    break;
                default:
                    System.out.println("Going to home page");
                    break;
            }
        } while (temp != taskMenu.getEscapeKey());

    }

    private void addTask() {
        Task t;
        YesNoMenu continuty = new YesNoMenu("Add more", "Do you Want to Add Task");
        int temp;
        boolean check;
        do {
            System.out.println(continuty);
            temp = continuty.getInput();
            switch (temp) {
                case 1:
                    t = tList.makeNewTask(pList);
                    check = tList.addNewTask(t);
//                    MailUtility.send("leminhthu96@gmail.com", "New Task", t.toString(), "text/plain");
                    break;
                case 2:
                    tList.printAllTask();
                    break;
                default:
                    System.out.println("Go back");
                    break;
            }
        } while (temp != continuty.getEscapeKey());
    }

    public void execSearch() {

        Menu search = new Menu("Search Menu", "Please Choose to use our queries");
        search.addOption("Print All Task By Leader");
        search.addOption("Print All Task By Status");
        search.addOption("Print task in Date Range");
        search.addOption("Sort By Status and view Average time");
        search.addOption("Compare Task");
        search.addOption("Task not completed by an user");

        System.out.println(search);
        int input = InputUltilities.getNextInt("Please enter your option");
        switch (input) {
            case 1:
                tList.printTaskByLeader();
                break;
            case 2:
                System.out.println(tList.getTaskByStatus());
                break;
            case 3:
                System.out.println(tList.getTaskByDateRange());
                break;
            case 4:
                tList.sortTaskByStatus();
                tList.printAllTask();
                System.out.println("Average Time taken: " + tList.getAverageTime() + " hour(s)");
                break;
            case 5:
                System.out.println("Compare Taskk for Equality");
                compareTasks();
                break;
            case 6:
                System.out.println(tList.lateTaskByUser());
                break;
            default:
                break;
        }

    }

    private void editTask() {
        if (tList.getSize() > 0) {
            System.out.println("EDIT THE INFORMATION OF 1 Task");
            String id = InputUltilities.getNextString("Enter ID here");
            String content = "";
            String field = InputUltilities.getNextString("Enter Field: ");
            switch (field) {
                case "name":
                    content = InputUltilities.getNextString("Enter new Name");
                    tList.editTaskNameByID(id, content);
                    break;
                case "status":
                    content = InputUltilities.getNextString("Enter new Status");
                    switch (content) {
                        case "Ongoing":
                            tList.editTaskStatusByID(id, Status.ONGOING);
                            break;
                        case "Cancelled":
                            tList.editTaskStatusByID(id, Status.CANCELLED);
                            break;
                        case "Paused":
                            tList.editTaskStatusByID(id, Status.PAUSED);
                            break;
                        case "Completed":
                            tList.editTaskStatusByID(id, Status.COMPLETED);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    System.out.println("Field Not Found");
                    break;
            }
            pList.printEmployeeInfoByID(id);
        } else {
            System.out.println("Not Available at the moment");
        }

    }

    public void compareTasks() {
        String id1 = InputUltilities.getNextString("Enter ID1 here");
        String id2 = InputUltilities.getNextString("Enter ID2 here");

        if (tList.getTaskByID(id1) == -1 || tList.getTaskByID(id2) == -1) {
            System.out.println("Cant perform action");
        } else {

            System.out.println("Are they equals");
            System.out.println(tList.getTask().get(tList.getTaskByID(id1)).equals(tList.getTask().get(tList.getTaskByID(id1))));
        }
    }

    private Menu mainMenu = new Menu("MAIN MENU", "WELCOME TO THE TASK MANAGEMENT, PLEASE SELECT YOUR ACTION");
    private Menu personMenu = new Menu("PERSON MENU", "YOU ARE NOW AT EMPLOYEE SECTION, PLEASE CHOOSE TO DO THE FOLLOWING");
    private Menu taskMenu = new Menu("TASK MENU", "YOU ARE NOW AT TASK SECTION, PLEASE CHOOSE TO DO THE FOLLOWING");

    private void initialize() {

        tList = (TaskList) SerializationUtility.load("data/", "taskList.txt");
        pList = (PeopleList) SerializationUtility.load("data/", "peopleList.txt");

        mainMenu.addOption("Manage Employee");
        mainMenu.addOption("Manage Task");
        mainMenu.addOption("Search On Task");

        personMenu.addOption("View All People");
        personMenu.addOption("Add Person");
        personMenu.addOption("Edit Person Info");
        personMenu.addOption("Delete Employee");

        taskMenu.addOption("View all Task");
        taskMenu.addOption("Add Task");
        taskMenu.addOption("Edit Information");
        taskMenu.addOption("Delete Task");

        TaskList backupList;
        try {
            backupList = (TaskList) tList.clone();
//            System.out.println(backupList);

        } catch (CloneNotSupportedException ex) {
        }
    }

}
