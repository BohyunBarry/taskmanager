/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import ultilitiesPackage.DateFormatUltilities;
import ultilitiesPackage.InputUltilities;

/**
 *
 * @author LmThu
 */
public class DataExtracter {

    public void printTaskByLeader(List<Task> taskList) {
        String leaderId = InputUltilities.getNextString("Please Enter the id of leader");
        for (Task _task : taskList) {
            if (_task.gettaskLeader().getId().equalsIgnoreCase(leaderId)) {
                System.out.println(_task);
            }
        }
    }

    public List<Task> getTaskByStatus(List<Task> taskList, Status status) {
        List<Task> taskByStatusList = new ArrayList<>();
        for (Task _task : taskList) {
            if (_task.getStatus().equals(status)) {
                taskByStatusList.add(_task);
            }
        }
        return taskByStatusList;
    }

    public List<Task> getTaskByDateRange(List<Task> taskList, Calendar dueOnMin, Calendar dueOnMax) {
        List<Task> taskByStatusList = new ArrayList<>();
        for (Task _task : taskList) {
            if (DateFormatUltilities.getDifference(_task.getDueOnDate(), dueOnMin) >= 0 && DateFormatUltilities.getDifference(dueOnMax, _task.getDueOnDate()) >= 0) {
                taskByStatusList.add(_task);
            }
        }
        return taskByStatusList;
    }

    public double getTaskAverageSecond(List<Task> taskList) {
        double sum = 0;
        for (Task _task : taskList) {
            sum += (double) DateFormatUltilities.getDifference(_task.getDueOnDate(), _task.getAddOnDate()) / 1000;
        }
        return sum / taskList.size();
    }

    public List<Task> getTaskByLeader(List<Task> taskList, Person leader) {
        List<Task> _taskList = new ArrayList<>();
        for (Task _task : taskList) {
            if (_task.gettaskLeader().equals(leader)) {
                _taskList.add(_task);
            }
        }
        return _taskList;
    }

    

}
