/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager;

/**
 *
 * @author LmThu
 */
public enum Status {
    CANCELLED,
    ONGOING,
    PAUSED,
    COMPLETED,
}
