/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import ultilitiesPackage.InputUltilities;

/**
 *
 * @author LmThu
 */
public class PeopleList implements Serializable {

    private static final long serialVersionUID = 956218136561734535L;
    private List<Person> people = new ArrayList<>();

    public PeopleList() {
    }

    public PeopleList(ArrayList<Person> people) {
        this.people = people;
    }

    public int getSize() {
        return people.size();
    }

    @Override
    public String toString() {
        String temp = "{People";
        for (Person p : people) {
            temp += p + "";
        }
        return temp + "}";
    }

    public Person makeNewPerson() {
        String name = InputUltilities.getNextString("Please Enter the Name of new employee name");
        String email = InputUltilities.getNextString("Please Enter your email address");
        String phone = InputUltilities.getNextString("Please Enter your phone Number");
        return (new Person(name, email, phone));
    }

    public void addNewPerson(Person p) {
        people.add(p);
    }

    public int getEmployeeByID(String id) {
        int i = 0;
        do {
            if (!(id.equals(people.get(i).getId()))) {
                i++;
            }
        } while ((i < people.size()) && (!(id.equals(people.get(i).getId()))));
        if (i >= people.size()) {
            return -1;
        } else {
            return i;
        }
    }

    public void printEmployeeInfoByID(String id) {
        int i = getEmployeeByID(id);
        if (getEmployeeByID(id) != -1) {
            System.out.println(people.get(i));
        } else {
            System.out.println("Cant Find the requested Employee");
        }
    }

    public boolean editEmployeeNameByID(String id, String name) {
        int i = getEmployeeByID(id);
        if (getEmployeeByID(id) != -1) {
            people.get(i).setName(name);
            return true;
        } else {
            System.out.println("Cant Find the requested Employee");
            return false;
        }
    }

    public void editEmployeeEmailByID(String id, String email) {
        int i = getEmployeeByID(id);
        if (getEmployeeByID(id) != -1) {
            people.get(i).setEmail(email);
        } else {
            System.out.println("Cant Find the requested Employee");
        }
    }

    public void editEmployeePhoneByID(String id, String phone) {
        int i = getEmployeeByID(id);
        if (getEmployeeByID(id) != -1) {
            people.get(i).setTel(phone);
        } else {
            System.out.println("Cant Find the requested Employee");
        }
    }

    public void deleteEmployeeInfoByID(String id) {
        int i = getEmployeeByID(id);
        if (getEmployeeByID(id) != -1) {
            people.remove(i);
        } else {
            System.out.println("Cant Find the requested Employee");
        }
    }

    public List<Person> getList() {
        return people;
    }

    public int getPeopleListSize() {
        return people.size();
    }

    public void printAllPeople() {

        if (people.size() > 0) {
            System.out.println(this.toString());
        } else {
            System.out.println("EMPTY");
        }

    }

    public void setAll(List<Person> _people) {
        people.addAll(_people);
    }
}
