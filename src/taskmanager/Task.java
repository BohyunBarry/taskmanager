/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taskmanager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import ultilitiesPackage.DateFormatUltilities;

/**
 *
 * @author LmThu
 */
public class Task implements Comparable<Task>, Cloneable,Serializable {

    private String taskId;
    private String taskName;
    private Person taskLeader;
    private List<Person> taskTeam = new ArrayList<>();
    private Calendar addOnDate;
    private Calendar dueOnDate;
    private String completeOnDate;
    private Status status;
    private boolean isNotifyOverdue;

    private static int lastId = 0;

    public Task() {
        taskName = "NA";
        lastId++;
        taskId = "TASK" + lastId;
        taskLeader = null;
        taskTeam = null;
        addOnDate = Calendar.getInstance();
        dueOnDate = Calendar.getInstance();
        status = Status.ONGOING;

    }

    public Task(String taskName, Person taskLeader, List<Person> taskTeam, Calendar addOnDate, Calendar dueOnDate) {
        lastId++;
        this.taskId = "TASK" + lastId;
        this.taskName = taskName;
        this.taskLeader = taskLeader;
        this.taskTeam = taskTeam;
        this.addOnDate = addOnDate;
        this.dueOnDate = dueOnDate;
        status = Status.ONGOING;
        isNotifyOverdue = false;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public Person gettaskLeader() {
        return taskLeader;
    }

    public List<Person> gettaskTeam() {
        return taskTeam;
    }

    public Calendar getAddOnDate() {
        return addOnDate;
    }

    public Calendar getDueOnDate() {
        return dueOnDate;
    }

    public String getCompleteOnDate() {
        return completeOnDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public void setTaskLeader(Person taskLeader) {
        this.taskLeader = taskLeader;
    }

    public void setTaskTeam(List<Person> taskTeam) {
        this.taskTeam = taskTeam;
    }

    public void setAddOnDate(Calendar addOnDate) {
        this.addOnDate = addOnDate;
    }

    public void setDueOnDate(Calendar dueOnDate) {
        this.dueOnDate = dueOnDate;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "{ID: " + taskId + " Task Name: " + taskName + " Task Leader:" + taskLeader + " Task Team:" + taskTeam
                + " Add On Date: " + DateFormatUltilities.getDateAsString(addOnDate) + " Due On Date: " + DateFormatUltilities.getDateAsString(dueOnDate) + " Status: " + status + "\r\n}";
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Task)) {
            return false;
        } else {
            Task pTemp = (Task) obj;
            return this.taskId.equals(pTemp.getTaskId()) && this.taskName.equals(pTemp.getTaskName());
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.taskName);
        return hash;
                
    }

    @Override
    public int compareTo(Task o) {
        if (this.status.ordinal() > o.getStatus().ordinal()) {
            return 1;
        } else if (this.status.ordinal() < o.getStatus().ordinal()) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
